Wayland is both a display architecture and display protocol which allows the presentation and composition of framebuffers. 

The Wayland architecture has three main components which implement a client-server architecture:
1. Wayland Client 
	* Are frame buffers sent to the graphics stack to be composited and sent. Are usually individual windows / widgets in a desktop environment
2. Wayland Compositor
	* Is the server in the Wayland architecture. It's responsible for multiplexing rendering and input devices to clients, as well as compositing clients into a final framebuffer for presentation
1. Wayland Protocol
	* The term Wayland describes the protocol / architecture utilized rather than any implementation. The Wayland protocol is what connects Wayland clients and compositor through Unix sockets

It replaces X11 by combining the X server (and other features it that have now been moved out of the X11 server, e.g. drivers and mode setting) and the X compositor into one singular program; the Wayland compositor

# Architecture

Based on a client-server architecture, with the compositor being the server. The compositor abstracts and multiplexes the hardware among clients; interfacing with `evdev` (inputs), [[Kernel Mode Setting (KMS)|KMS]] (mode setting), and [[Direct Rendering Manager (DRM)|DRM]] (called the DRM master)

The Wayland client utilizes the _direct rendering_ technique to render it's content. This means that the client is responsible for allocating framebuffers using GBM and sharing that framebuffer with the compositor. The client is also responsible for  using OpenGL (either hardware or software back end implementations) directly   to render content to those framebuffers for the compositor.

![[wayland-architecture-diagram.png]]

Wayland is a _descriptive_ protocol rather than _prescriptive_; the client gives an image and furnishes as much context and information to the compositor (e.g., a popup window, an overlay for the desktop environment, a top-most window etc.). This is contrasted to the _prescriptive_ X compositor where clients are given complete control over input/output devices and total reign over how it's image is rendered, often leading to incoherent window implementations in desktops. In other words, Wayland relies on the server implementing good semantics, not the client dictating 
# Debugging
`wayland-info` is a client application that connects to and shows information about the Wayland  compositor such supported Wayland protocols and versions 

`WAYLAND_DEBUG=1` can be used to see the requests sent between the Wayland client and server (e.g. `WAYLAND-DEBUG=1 simple-gl`)

# Implementation

libwayland-{display,server}: Wayland protocol marshaling; thin wrappers for the communications protocol b/w the client and display server

All Wayland compositors implement the same protocols that are marshaled by by libwayland. The server-behavior, composition and the window management policies that would be separate programs in X are all dictated by the Wayland compositor.

The Wayland compositor advertises extensions to Wayland clients that connect to it. Most of the functionality in a Wayland client are supported via extensions. The core Wayland protocol is somewhat useless, as the intention is to support complex behaviors with extensions, similar to how Vulkan requires extensions to actually make it useful.

A Wayland client begins by polling the compositor for what extensions are supported/should be used.
- _wl_buffer_ is a handle to some frame buffer object somewhere
- _wl_surface_ is a (sub)window; is a container for  _wl_buffers_ from the client and a receiver for input from the compositor. 
- _xdg_surface_ adds desktop environment semantics on top of the _wl_surface_: size negotiation, title, icon, etc.
- _xdg_toplevel_ wraps an _xdg_surface_ and provides further semantics: is a pop-up window, a dialog window, or application window, right-click levels, etc.
- _wl_seat_ provides input interaction

![[wayland-display-pipeline-diagram.png]]

The Wayland client loop. It has a very similar workflow to KMS.
1. Client creates a server and annotates it with metadata
2. Waits for server to tell it what size it should be
3. Client allocates buffer (SHM for CPU rendering and dmabuf with GPU rendering)
4. Draw first buffer, attach to surface, commit surface
5. Wait for compositor to tell you when to paint the next frame
6. Draw next buffer, attach to surface, commit surface
7. Repeat 4-6

The initial setup can be verbose and messy because of the extensions interface, but also means that compositor-intensive paths are properly designed out.

## Implementation Resources

- `wayland-info` tells you what extensions are available as a Wayland client
- `wlhax` is a client proxy to monitor requests & events from the server
- Weston has `simple-smh`, `simple-egl`, etc. sample clients on GitLab
	- `weston-debug` introspects Weston's internal details/logs

## GPUs: Using EGL and OpenGL ES

OpenGL (ES) is a pure rendering API for accelerated 3D. Clients provide vertex, fragment, and texture data as well as computational shaders. Originally, X11 mediated OpenGL commands (i.e., X11 actually implemented the OpenGL calls) through the GLX extension.

Nowadays, the [[Direct Rendering Infrastructure (DRI)]] allowed clients to directly access the GPU whilst not conflicting with the X server. DRI2 allowed clients to have their own memory areas. DRI is used as a shorthand for accelerated rendering. DRI2 is used as a shorthand for 'modern' window system.

EGL abstracted GLX to other window systems (e.g., Wayland, Android, etc.). It serves as the "glue" that connects GL and window systems. EGL manages external resources for OpenGL (e.g., windows to be displayed). It provides a frame-time barrier; rendering only occurs when the client permits it. This implementation is not great since EGL does not have an event infrastructure to give EGL context to what's going on. 

![[wayland-egl-opengl-diagram.png]]

This is where GBM comes in as a side channel for compositors.

## GBM

The [[Generic Buffer Management (GBM)]] serves as a side channel to pass BOs between the compositor and DRM
## GPUs: Vulkan

Vulkan interacts with Wayland almost exactly like OpenGL. The Vulkan core API operates exactly like OpenGL ES and the Vulkan Window System Integration (WSI) layer is simply EGL. 
## dmabuf

There needs to be an operation that permits sharing buffers between different subsystems and processes; in this context, with the rendering API and the window system. GEM buffer objects (BO) are user-local: not global nor across devices. dmabuf (a.k.a. "prime") exports a file descriptor representing the BO to pass it around 

Each Linux subsystem has IOCTLs to import dmabuf as a native buffer and export a native buffer as a dmabuf. Each protocol API (.e.g., Wayland, EGL) has support for transporting dmabuf + metadata

# Realizing The Pipeline

## The Client

Simple EGL-based client as the example:
1. Client connects to the Wayland compositor, creates surface with the name `org.freedesktop.weston.simple-egl`
2. Creates EGL wrapping a Wayland surface
3. Wayland server tells EGL client how to allocate buffer, EGL allocates via DRM, exports via dmabuf
4. OpenGL ES renders to buffer
5. wl_buffer created from the dmabuf is attached to the surface
## The Server

The server has the same behavior, just in reverse
1. Server notes client name , decides where to place it
2. Recieves dmabuf and creates a wl_buffer from it
3. Imports wl_buffer into EGL and/or KMS
4. Waits for next repaint window
5. Calculates overall scene graph  to determine presentation
6. If GPU, uses OpenGL ES to composite single image
7. If via KMS, places scene graph into KMS planes/etc.

## An Efficient Display Pipeline

- Clients allocate their own buffers and have no direct knowledge of the final displays.
- DRM/KMS cannot express all display hardware constraints.
- _Sometimes_ the display hardware can composite itself Whether this is possible depends on every factor - provided scene graph, client buffers, global system status.
- It's difficult in advanced to have a static and useful display controller pipeline. point of the stack must asynchronously transport useful information and hints: a feedback loop.
- KMS atomic modesetting provides a 'check-only' mode: i.e., 'can you display this configuration?'
- Without knowing the optimal configuration, must brute force
- For every new configuration (window moved. added, etc.) try to map every available surface to every available plane
- When we find a working configuration, use that 
- This brute force method is possible because the 'check-only' is required to be fast enough
- Weston is currently state of the art here, other Wayland compositors are still catching up

# TODO
* https://wayland.freedesktop.org/docs/html/index.html
* https://drewdevault.com/2017/06/10/Introduction-to-Wayland.html
* https://wayland-book.com/introduction.html
* https://drewdevault.com/2017/06/10/Introduction-to-Wayland.html
* https://www.youtube.com/watch?v=haes4_Xnc5Q&list=PLdND2EtevwMQKGDKgxwVyVEJLkpMvKwhg&index=18
* https://blog.mecheye.net/2014/06/xdg-shell/
* 
* https://drewdevault.com/2018/02/17/Writing-a-Wayland-compositor-1.html
	* and the following series
* https://drewdevault.com/2018/07/29/Wayland-shells.html
* https://wiki.alopex.li/WritingAWaylandCompositorInRust
* https://jan.newmarch.name/Wayland/index.html
* https://streaming.media.ccc.de/jev22/relive/49255