# Introduction

Is a framework for userspace drviers that provide an implementation for graphics APIs
* Has hardware vendor specific OpenGL/Vulkan & GLSL 
* Are considered [[Direct Rendering Infrastructure (DRI)]] drivers 
* Common [[Gallium3D]]  and [[NIR]] infrastructure

Mesa is a framework since much of the code is hardware agnostic. For example in OpenGL's state-based APIs many API calls do not have an immediate effect, they only modify values and those values are pushed to the hardware on draw calls. Since these APIs do not interact with the hardware, their implementation can be shared by multiple drivers, and each driver implements the draw call.

# Support
Supports GPUs that have a [[Direct Rendering Manager (DRM)|DRM]] render driver
Implements software rendering fallbacks
Supports GPU video decoding through VDPAU, VAAPI or OMX
Supports compute via OpenCL (clover driver)

# Examining A Mesa Driver

![[mesa-3d-stack.png]]

## How are APIs Implemented
Here is an example of a stack trace of a `glDrawArrays()` call.

```
`brw_upload_state () at brw_state_upload.c:651`
`brw_try_draw_prims () at brw_draw.c:483`
`brw_draw_prims () at brw_draw.c:578`
`vbo_draw_arrays () at vbo/vbo_exec_array.c:667`
`vbo_exec_DrawArrays () at vbo/vbo_exec_array.c:819`
`render () at main.cpp:363`
```

Notice `glDrawArrays()` is actually `vbo_draw_arrays()` and `vbo_draw_arrays()`. These are hardware independent functions and are reused by other drivers in Mesa. These generic functions are all over Mesa and would usually do things like checks for API errors, input formatting, any pre-processing for later stages, etc.

The `brw_draw_prims()` and `brw_upload_state()` are hooks in the DRI driver which call hardware-specific functionality such as uploading state, configuring shader stages, etc.

## Registering Driver Hooks

Looking at the source code for `gDrawArrays()` reveals how the DRI driver hooks are implemented:
```c
static void
vbo_draw_arrays(struct gl_context *ctx, GLenum mode, GLint start,
                GLsizei count, GLuint numInstances, GLuint baseInstance)
{
   struct vbo_context *vbo = vbo_context(ctx);
   // (...)
   vbo->draw_prims(ctx, prim, 1, NULL, GL_TRUE, start, start + count - 1,
                   NULL, NULL);
   // (...)
}
```

The hook is in `draw_prims()` inside of `vbo_context`. More trivial searches shows how the hook is set up in `brw_draw_init()`:
```c
void brw_draw_init( struct brw_context *brw )
{
   struct vbo_context *vbo = vbo_context(ctx);
   // (...)
   
   /* Register our drawing function: */
   
   vbo->draw_prims = brw_draw_prims;
   // (...)
}
```

Adding a break point there reveals that Mesa calls into the DRI driver when setting up the OpenGL context. There is when the driver will register various hooks:
```
`brw_draw_init () at brw_draw.c:583`
`brwCreateContext () at brw_context.c:767`
`driCreateContextAttribs () at dri_util.c:435`
`dri2_create_context_attribs () at dri2_glx.c:318`
`glXCreateContextAttribsARB () at create_context.c:78`
`setupOpenGLContext () at main.cpp:411`
`init () at main.cpp:419`
`main () at main.cpp:477
```

Hardware vendors provide an OpenGL implementation registeres its hooks for context creating by assigning a global variable with the hooks they need. Here is an example in the Intel DRI driver:
```cpp
static const struct __DriverAPIRec brw_driver_api = {
   .InitScreen           = intelInitScreen2,
   .DestroyScreen        = intelDestroyScreen,
   .CreateContext        = brwCreateContext,
   .DestroyContext       = intelDestroyContext,
   .CreateBuffer         = intelCreateBuffer,
   .DestroyBuffer        = intelDestroyBuffer,
   .MakeCurrent          = intelMakeCurrent,
   .UnbindContext        = intelUnbindContext,
   .AllocateBuffer       = intelAllocateBuffer,
   .ReleaseBuffer        = intelReleaseBuffer
};

PUBLIC const __DRIextension **__driDriverGetExtensions_i965(void)
{
   globalDriverAPI = &brw_driver_api;

   return brw_driver_extensions;
}
```
Notice there are two types of hooks:
1. Hooks needed to link the driver not the DRI implementation (entry points for driver in Mesa)
2. Hooks related to the hardware implementation of OpenGL commands

Writing a new DRI driver is as simple as writing an implementation for all of these hooks. The rest are implemented in Mesa and reused across multiple devices.

# Gallium3D
[[Gallium3D|Gallium3D]] is a set of interfaces and supporting libraries intended to make programming graphics drivers easier.

As of 2014, there are two kinds of Mesa drivers: the classic drivers (not based on Gallium3D) and the new Gallium drivers. Classically the Mesa hardware drivers were tightly coupled with the OpenGL impelementation. The Gallium3D framework separates these concerns by providing an API that exposes hardware functions in modern GPUs rather than targetting one specific graphics API. This allows support for other graphics APIs such as Direct3D and Vulkan.

Other benefits include supporting various OS's further separating the part of the driver that depends on the OS

## Gallium and LLVM
Writing a modern GPU driver requires a lot of native code generation and optimization. OpenGL also drivers requires a GLSL compiler. Rather than implement their own compiler toolchain, LLVM was introduced into Mesa.

LLVM allows Mesa devekioers to bring new optimizations to shaders and produce better native code, which is critical to performance. This would also eliminate much of the duplicate code for compiling GLSL code.

How Gallium plugs into LLVM is simple: Mesa provides developers an LLVM frontend for GLSL, LLVM then produces IR code and performs optimizations, then hardware vendors provide an LLVM backend for their target GPU

## Hardware and Software Drivers

Mesa includes software drvers. Software drivers are useful for various reasons:
* For developing and testing purposes, when you want to take the hardware out of the equation. From this point of view, a software representation can provide a reference for expected behavior that is not tied or constrained by any particular hardware. For example, if you have an OpenGL program that does not work correctly we can run it with the software driver: if it works fine then we know the problem is in the hardware driver, otherwise we can suspect that the problem is in the application itself.
* To allow execution of OpenGL in systems that lack 3D hardware drivers. It would obviously be slow, but in some scenarios it could be sufficient and it is definitely better than not having any 3D support at all.


# Driver loading and querying in Mesa

## Driver Selection
On systems that use Mesa drivers (no Nvidia graphics), there are multiple driver packages for different families of cards. Within each driver package several drivers for various GPUs. Here is an example of a Mesa DRI driver package for a system with an Intel GPU:

```
# dpkg -L libgl1-mesa-dri:amd64
(...)
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_radeonsi.so
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_r600.so
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_nouveau.so
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_vmwgfx.so
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_r300.so
/usr/lib/x86_64-linux-gnu/gallium-pipe/pipe_swrast.so
/usr/lib/x86_64-linux-gnu/dri/i915_dri.so
/usr/lib/x86_64-linux-gnu/dri/i965_dri.so
/usr/lib/x86_64-linux-gnu/dri/r200_dri.so
/usr/lib/x86_64-linux-gnu/dri/r600_dri.so
/usr/lib/x86_64-linux-gnu/dri/radeon_dri.so
/usr/lib/x86_64-linux-gnu/dri/r300_dri.so
/usr/lib/x86_64-linux-gnu/dri/vmwgfx_dri.so
/usr/lib/x86_64-linux-gnu/dri/swrast_dri.so
/usr/lib/x86_64-linux-gnu/dri/nouveau_vieux_dri.so
/usr/lib/x86_64-linux-gnu/dri/nouveau_dri.so
/usr/lib/x86_64-linux-gnu/dri/radeonsi_dri.so
(...)
```

Mesa is smart enough to know which driver is the right one to load for a particular GPU, and automatically selects it when `libGL.so` is loaded. This part of Mesa is dubbed the *loader*. You can point Mesa to look for suitable drivers in a specific directory other than the default, or use a software driver [using various environment variables](http://www.mesa3d.org/envvars.html "Mesa 3D environment variables").

## Check What Driver is Mesa Loading

If you want to know exactly what driver Mesa is loading, you can instruct it to dump this (and other) information to _stderr_ via the _LIBGL_DEBUG_ environment variable (only works if Mesa loads the driver):

```
# LIBGL_DEBUG=verbose glxgears
libGL: screen 0 does not appear to be DRI3 capable
libGL: pci id for fd 4: 8086:0126, driver i965
libGL: OpenDriver: trying /usr/lib/x86_64-linux-gnu/dri/tls/i965_dri.so
libGL: OpenDriver: trying /usr/lib/x86_64-linux-gnu/dri/i965_dri.so
````

The code in [src/loader/loader.c](https://gitlab.freedesktop.org/mesa/mesa/-/blob/main/src/loader/loader.c)  *loader_get_driver_for_fd* is responsible for detecting the right driver to use. This recieves the device fd as an input parameter that is acquired by calling _DRI2Connect()_ as part of the DRI bring-up process. Then the actual driver file is loaded in _glx/dri_common.c (driOpenDriver)_.

We can also obtain a more descriptive indication of the driver we are loading by using the _glxinfo_ program that comes with the _mesa-utils_ package (with a card that uses a Mesa driver, the device is prefixed with "*Mesa DRI"*):

```
# glxinfo | grep -i "opengl renderer"
OpenGL renderer string: NVIDIA GeForce RTX 3080 Ti/PCIe/SSE2
```

## Forcing a Software Driver

Mesa provides an environment variable to force the use of software drivers.:

```
# LIBGL_DEBUG=verbose LIBGL_ALWAYS_SOFTWARE=1 glxgears
libGL: OpenDriver: trying /usr/lib/x86_64-linux-gnu/dri/tls/swrast_dri.so
libGL: OpenDriver: trying /usr/lib/x86_64-linux-gnu/dri/swrast_dri.so
```

As we can see, setting _LIBGL_ALWAYS_SOFTWARE_ will make the leader select the software driver (_swrast_). You can validate this with _glxinfo_ like before:

```
# LIBGL_ALWAYS_SOFTWARE=1 glxinfo | grep -i "opengl renderer"`
`OpenGL renderer string: Software Rasterizer`
```

## Querying the Driver for OpenGL features

The _glxinfo_ program also comes in handy to obtain information about the specific OpenGL features implemented by the driver. If you want to check if the Mesa driver for your hardware implements a specific OpenGL extension you can inspect the output of glxinfo and look for that extension:

```
# glxinfo | grep GL_ARB_texture_multisample
```

You can also ask glxinfo to include hardware limits for certain OpenGL features including the _-l_ switch. For example:

```
# glxinfo -l | grep GL_MAX_TEXTURE_SIZE
GL_MAX_TEXTURE_SIZE = 8192
```

# Source Code

## Source Code Tree

There is documentation providing an overview of the source code for Mesa3D on the [gitlab repository](https://docs.mesa3d.org/sourcetree.html). Here is a brief overview complimenting that onfirmation with some more detail"

- In _src/glx/_ we have the OpenGL bits relating specifically to X11 platforms, known as GLX. So if you are working on the GLX layer, this is the place to go. Here there is all the stuff that takes care of interacting with the XServer, the client-side DRI implementation, etc.

- _src/glsl/_ contains a critical aspect of Mesa: the GLSL compiler used by all Mesa drivers. It includes a GLSL parser, the definition of the Mesa IR, also referred to as GLSL IR, used to represent shader programs internally, the shader linker and various optimization passes that operate on the Mesa IR. The resulting Mesa IR produced by the GLSL compiler is then consumed by the various drivers which transform it into native GPU code that can be loaded and run in the hardware.

- _src/mesa/main/_ contains the core Mesa elements. This includes hardware-independent views of core objects like textures, buffers, vertex array objects, the OpenGL context, etc as well as basic infrastructure, like linked lists.

- _src/mesa/drivers/_ contains the actual classic drivers (not Gallium). DRI drivers in particular go into _src/mesa/drivers/dri_. For example the Intel i965 driver goes into _src/mesa/drivers/dri/i965_. The code here is, for the most part, very specific to the underlying hardware platforms.

- _src/mesa/swrast*/_ and _src/mesa/tnl*_/ provide software implementations for things like rasterization or vertex transforms. Used by some software drivers and also by some hardware drivers to implement certain features for which they don’t have hardware support or for which hardware support is not yet available in the driver. For example, the _i965_ driver implements operations on the accumulation and selection buffers in software via these modules.
- _src/mesa/vbo/_ is another important module. Across its various versions, OpenGL has specified many ways in which a program can tell OpenGL about its vertex data, from using functions of the _glVertex*()_ family inside _glBegin()/glEnd()_ blocks, to things like vertex arrays, vertex array objects, display lists, etc… The drivers, however, do not need to deal with all this, Mesa makes it so that they always receive their vertex data as collection of vertex arrays, significantly reducing complexity on the side of the driver implementator. This is the module that takes care of managing all this, so no matter what type of drawing you GL program is doing or how it specifies its vertex data, it will always go through this module before it reaches the driver.

- _src/loader/_ contains the Mesa driver loader, which provides the logic necessary to decide which Mesa driver is the right one to use for a specific hardware so that Mesa’s _libGL.so _ can auto-select the right driver when loaded.

- _src/gallium/_ contains the _Gallium3D_ framework implementation. This is the place where you will find the various Gallium drivers in development (inside _src/gallium/drivers/_), like the various Gallium ATI/AMD drivers, Nouveau or the LLVM based software driver (_llvmpipe_) and the Gallium state trackers.

## Building Mesa

Write this section using the following resources when neccessary:
- https://blogs.igalia.com/itoral/2014/09/15/setting-up-a-development-environment-for-mesa/



# Mesa Shader Compilation

![[mesa-glsl-shader-compilation-pipeline-1.png]]
![[mesa-glsl-shader-compilation-pipeline-2.png]]
# Resources
- https://www.reddit.com/r/linux/comments/36zcsw/a_brief_introduction_to_the_linux_graphics_stack/
	- https://blogs.igalia.com/itoral/2014/08/08/diving-into-mesa/
- [Embedded Graphics Drivers in Mesa (ELCE 2019)](https://www.youtube.com/watch?v=By_XgayTSrY)
- 