- KMS is a sub-part of DRM API and is responsible for configuring and driving the display
- The "mode" in this context is a particular display configuration the display  is capable of running, i.e. encoders, connectors, resolutions + refresh rate, HDR output metadata, etc.
	
KMS is used by the [[Direct Rendering Manager (DRM)|DRM master]]; programs with exclusive access to output that need low-level access to the GPU
- Display servers
- VR and XR

# KMS Objects

This is a diagram of the components of the DRM KMS stack. Note that this diagram is partially misleading since some components can have multiple inputs/outputs despite not depicting so. Each of these nodes represent a real KMS object that can be utilized in code.


![[kernel-mode-setting-pipeline-1.png]]

![[kernel-mode-setting-pipeline-2.png]]

## Framebuffer
* **Framebuffers** are abstract memory objects that provide a source of pixels to scan-out to a CRTC
	* Applications explicitly request the creation of frame buffers through `ioctl` calls.
* Relies on the underlying memory manager for allocating buffers (e.g. GEM or TTM) for user-space buffer management (i.e., KMS does not allocate memory)
	* Memory location of the framebuffer depends on the situation:
		* System memory  ([[Shared Memory (SHM)|SHM]]) or dedicated graphics memory ([[Embedded OpenGL (EGL)|EGL]])
		* Paged (fragmented) or contiguous memory
		* Hardware-side **MMU** (CPU-side), **IOMMU** (device-side), and **DMA** controllers are responsible for creating virtually contiguous memory from physically fragmented memory
		* System-side **bus mapping** allows the CPU to map graphics memory to a virtual address location and access that graphics memory
		* **Caching** speeds up data accesses but risks data races, e.g., CPU write operations to the cache does not get flushed at all nor at the correct time, and vice versa with the graphics device (**cache coherency**)
	* Default implementation available for GEM objects using Contiguous Memory Allocator (CMA): `drivers/gpu/drm/drm_fb_cma_helper.c

### DRM Formats
Framebuffers do not contain metadata, formats, modifier, compression , etc. must be stored separately from the framebuffer

DRM formats describe how the pixels are laid out in the framebuffer. The list of all available formats is located in `/usr/include/libdrm/drm_fourcc.h`. Some very common formats such as `XRGB8888` and `ARGB8888` are the most likely to be supported (note how these are also [Vulkan formats](https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VkFormat.html)).

Note that DRM formats are stored in little-endian mode. For example, The value \#112233FF (R=0x11, G=0x22. B=0x33, A=0xFF) in format `ARGB8888` is stored as {0x33, 0x22, 0x11, 0xFF}.

## Planes
 * A **plane** represents an image layer that can be blended with or overplayed on top of a CRTC during the scan-out process.
	 * Do **NOT** confuse a plane with a framebuffer, planes represent very specific things in KMS and are part of the display controller
	 * The input is one (or more) framebuffers for double/triple buffering and [page flipping](https://en.wikipedia.org/wiki/Multiple_buffering#Page_flipping)
* Where pixel mixing occurs; specifies, cropping, rotation, scaling, format, transformation etc. of the image in the visible area of the display pipeline.
	* Planes are composited in a FIFO operation to the CRTC
	* A plane must be configured according to the display mode in the CRTC 
* Three different types of planes under the [[Direct Rendering Manager (DRM)|Universal Planes]] API:
	* `DRM_PLANE_TYPE_PRIMARY` (mandatory, one per CRTC):
		* Used by CRTC to store framebuffer
		* Typically used for background images or graphics content
	* `DRM_PLANE_TYPE_CURSOR` (optional, one per CRTC):
		* Used to display a cursor (mouse pointer)
	* `DRM_PLANE_TYPE_OVERLAY` (optional, 0 to N per CRTC):
		* Used to take advantage of  hardware accelerated composition
		* Typically used to display windows with dynamic content
		* In case of multiple CRTCs in display controller, the overlay planes can often be dynamically attached to a specific CRTC when required

## CRTC
* A **CRTC** represents the overall display pipeline.
	* Stands for CRT Controller, though this does not apply anymore
	* An abstraction for the display controller itself; the display mode is attached to the CRTC
* The CRTC is responsible for generating timings and pixel streaming
	* Pixel streaming refers to receiving and blending one, multiple, or no planes into a final framebuffer that is scanned out to the display
		* The CRTC is explicitly configured by the DRM driver on how to build the final buffer, on top of satisfying the current display mode
	* Panels / Monitors are controlled by specific timings; the CRTC is responsible for sending the pixels in correct orders, rates, etc. for the display hardware
		* Often works by a syncronous FIFO
*  The CRTC outputs to one or more encoders
	* The CRTC does not support all connectors, and therefore needs an intermediary, encoders
	* The CRTC can also be directly scan-out to multiple connectors to display the same planes across multiple connectors; called _screen cloning_

## Encoder
* An **encoder** represents the connecting element b/w the CRTC (i.e., the overall pixel pipeline and display controller representation) and the connectors (generic sink entity).
	* Is the display interface layer; an encoder takes pixel data from a CRTC and converts it to a format suitable for any attached connector.
		* For example, if the display is connected through HDMI using YUV color format, the encoder will be an HDMI encoder receives a framebuffer from the CRTC and format them for the display
	* Exposed UABI for encoders are implemented incorrectly in drivers. Because of this, encoders are considered an outdated construct and are usually ignored, save for custom use cases
		* Any mode set request from user space directly connects a connector w/ a CRTC. therefore drivers can use them however they wish.
		* Can be used for code sharing, but it's better to use a `drm_bridge` instead.
	* The output is a connector, and an encoder can drive multiple connectors.

## Bridge
* A **bridge** represents a virtual device that hangs on to an encoder to change from one display phase to another, i.e. interface transcoding
	* Not always present in the display pipeline
* Useful when one encoder entity is not enough to represent the entire encoder chain
	* E.g., supporting DP alt-mode over USB-C
	  https://www.uwsg.indiana.edu/hypermail/linux/kernel/2307.3/08429.html
	* E.g., an embedded panel uses VGA but the display controller on the SoC does not have a VGA encoder, a bridge is used to convert an HDMI or Display Port encoder to the embedded panel
* Provides additional hooks to get the desired output at the end of the encoder chain
	* Can represent an external encoder accessible through a bus

## Connector
* A **connector** is the general abstraction for the physical connector on a GPU, or any display sink which connects to anything that can display pixels (i.e., embedded displays)
* The connector reads all of the available modes from the display it is connected to
	* Reads Extended Display Identification Data (EDID)
* As apposed to all other KMS objects representing hardware (e.g., CRTC, encoder, or plane abstractions), connectors can be hot plugged at runtime.
	* The list of connects can change at runtime, e.g., connecting a dock with an HDMI output via USB-C 
* Connectors can have different possible encoders, but the kernel driver chooses which encoder to use for each connector

## Display
The final step in the display pipeline is emitting / reflecting light in accordance to the pixel data. There are two similar, yet distinct technologies (in regards to KMS and to each other) which have implications for how they are driven.
### Panel
Refers to a display surface that is always connected to a system; smartphone, tablet, laptop. In other words a screen that is never disconnected

### Monitor
A peripheral that integrates a panel that is intended to be disconnected and re-connected, among many different ports.

## Object Properties

Objects can be enumerated in KMS. It's possible to find all connectors, encoders, etc. in a mode setting program. All KMS objects also have a list of properties, and their properties can be enumerated as well.

Object properties are used to extend DRM / KMS capabilities without changing the objects itself (Linux is very particular about ABI compatibility). For example, EDID, VRR, and HDR capabilities are stored as object properties rather in the KMS objects themselves.


# Kernel Mode Setting

To preform mode setting, you need to open a DRM display device and enumerate through the KMS objects (i.e., connectors, CRTCs, planes, etc.).
Mode setting is performed backwards, from the connector to ensure that all proceeding KMS objects support the chosen connector and mode. 

The general steps to performing mode setting is as follows:
1. Read modes:
	1. `modetest` tool allows you to set modes through KMS, and also outputs available encoders, connectors, and display configurations (e.g., resolution, refresh rate. Extended Display Identification Data (EDID), etc.)
	2. This info is retrieved by parsing the connector info where the display is connected to the card
		1. `ls /sys/class/drm/` for the list of connectors
2. Configure Controller
	1. The CRTC is an abstraction for the display controller itself.
		1. Configure the CRTC to build the buffer to send to the display based on the display mode
		2. It may accept several planes / frame buffers and composite them into one frame buffer for the display 
	2. Part of the DRM/KMS stack
		1. `ls /sys/kernel/debug/dri/0` to see all of the descriptors about card connections, CRTC instances, and metadata
		2. Run  `cat /sys/kernel/debug/dri/0/name` and `cat /sys/kernel/debug/dri/0/state` to see how the display is configured in DRI
			2. Notice how there is only one CRTC connection for the HDMI output that is owned by `kwin_wayland`
1. Create Frame Buffers
	1. In the configuration tell the display controller to map the frame buffer to a CRTC plane and display it 


## modetest

![[kernel-mode-setting-procedure-chart.png]]


- For debugging a DRM driver, the file `/sys/module/drm/parameters/debug` can be sent flags
	- `echo 0x1ff > /sys/module/drm/parameters/debug`

Compose Multiple Pixel Buffers
* Only a single application may drive KMS and the display by design, dubbed DRM master
	* To support multiple frame buffers, a window compositor serves as the DRM master
## kmscube

kmscube is a little demonstration program for how to drive bare metal graphics without a compositor using DRM/KMS, GBM (graphics buffer manager) and EGL for rendering content using OpenGL or OpenGL ES. This project focuses on the GPU in mode setting.

https://gitlab.freedesktop.org/mesa/kmscube/
## kms-quads

kms-quads is a simple and self-explainatory example of how to use the Linux kernel's KMS API to drive graphical displays. This project focuses on timing in mode setting.

https://gitlab.freedesktop.org/daniels/kms-quads

# Dumb Buffer API

The GEM API doesn't standardize GEM object creation and leaves it to driver specific ioctl calls.  While not an issue for full-fledged graphics drivers, this made DRM-based early boot graphics (e.g., fbdev kernel init journal) unnecessarily complex.

Dumb GEM objects partially alleviate the problem by providing a standard API to create dumb buffers of arbitrary size that can be used for scan-out. These buffers can then be used to create KMS frame buffers for CPU rendering with `mmap` for simple CPU rendering. GPU access to these buffers is often not possible and, therefore, are not suitable for complex compositions and renderings.

# Atomic Mode Setting

The atomic mode setting interface provides a check ("will this succeed?") before committing the changes to the display stack. This check is important, because constraints of the display stack aren't immediately exposed. Once the atomic commit succeeds, content is visible until told otherwise.

DRM / KMS is a frame-by-frame, not a producer-consumer pipeline

# Resources
* [Linux GPU Driver Developer’s Guide - Kernel Mode Setting (KMS)](https://www.kernel.org/doc/html/v4.17/gpu/drm-kms.html#0)
* [Navigating the Linux Graphics Stack - Michael Tretter, Pengutronix](https://www.youtube.com/watch?v=S_rqgyId9YE) 
* [The DRM/KMS subsystem from a newbie’s point of view - Boris Brezillon, Free Elections](https://events.static.linuxfound.org/sites/events/files/slides/brezillon-drm-kms.pdf)
- [Graphics: A Frame's Journey - Daniel Stone, Collabora](https://www.youtube.com/watch?v=nau2dgdXWOk)
# TODO
* https://manpages.debian.org/unstable/libdrm-dev/drm-kms.7.en.html
* https://wiki.st.com/stm32mpu/wiki/DRM_KMS_overview#modetest_-28DRM-2FKMS_test_tool-29
* https://www.youtube.com/watch?v=haes4_Xnc5Q&list=PLdND2EtevwMQKGDKgxwVyVEJLkpMvKwhg&index=21
* https://manpages.debian.org/testing/libdrm-dev/drm-memory.7.en.html
* https://landley.net/kdocs/htmldocs/drm.html#drm-kms-init
* https://events.static.linuxfound.org/sites/events/files/slides/brezillon-drm-kms.pdf
* https://bootlin.com/doc/training/graphics/graphics-slides.pdf
* https://www.kernel.org/doc/Documentation/fb/framebuffer.txt
