
# TODO
* https://github.com/KhronosGroup/Vulkan-Guide
* https://fuchsia.googlesource.com/third_party/vulkan_loader_and_validation_layers/+/refs/heads/upstream/160811b-merge-master-to-android_layers/loader/LoaderAndLayerInterface.md
* https://chromium.googlesource.com/external/github.com/KhronosGroup/Vulkan-Loader/+/HEAD/loader/LoaderAndLayerInterface.md
* https://www.youtube.com/watch?v=P0piLVqEi7Q
* https://www.collabora.com/news-and-blog/blog/2018/03/23/a-new-era-for-linux-low-level-graphics-part-2/
* https://www.collabora.com/news-and-blog/blog/2022/03/23/how-to-write-vulkan-driver-in-2022/
	* https://www.collabora.com/news-and-blog/blog/2023/02/02/exploring-rust-for-vulkan-drivers-part-1/
* https://www.collabora.com/news-and-blog/blog/2023/03/10/implementing-vulkan-extensions-for-nvk/
* https://www.youtube.com/watch?time_continue=127&v=v0uUEePI1IA&embeds_euri=https%3A%2F%2Fwww.google.com%2F&source_ve_path=MjM4NTE
* https://github.com/KhronosGroup/Vulkan-Loader/blob/main/docs/LoaderInterfaceArchitecture.md#who-should-read-this-document
* https://archive.org/details/GDC2016Ginsburg