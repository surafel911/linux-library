Neutral Intermediate Representation (NIR) is the optimizing compiler stack that sits at the core of most Mesa drivers’ shader compilers.

## A New Compiler IR for Mesa
https://www.gfxstrand.net/faith/projects/mesa/nir-notes/


## GLSL-IR vs NIR

- GLSL-IR is typed, NIR is typeless  
- GLSL-IR is based on expression trees, NIR is a flat IR  
- More explicit control-flow in NIR  

NIR makes it much easier to write lowering/optimization  
passes on the IR

## Why NIR
- Reduce frontend code duplication  
- Leverage Vulkan work for ARB_gl_spirv and OpenGL 4.6  
- NIR is better suited for representing new features (e.g. 16 bit)  
- NIR is suited for code transforms  
	- Simplifies driver-specific optimization flows  
	- Enables hardware-specific optimization passes (e.g. for gfx9 merged shaders)

![[amd-mesa-driver-nir-pipeline.png]]