
The Graphics Execution Manager (GEM) does not actually manage any execution, it just manages memory. GEM manages buffer objects allocated by the kernel or user space (depends on context) for use in the graphics stack. There is no generic BO-allocation API due to hardware divergence (e.g., unified memory vs VRAM) and software divergence (kernel space vs user space allocations, i.e., who creates and backs the memory store).

GEM buffer objects (BO) are used for pixel buffers, vertex/compute data, firmware, etc. They are instrumental in transferring between data between the graphics APIs, window systems, and DRM/KMS subsystems. No metadata is attached to the BO itself. 

For simplier memory allocations, KMS dumb buffers are utilized.

https://lwn.net/Articles/283798/