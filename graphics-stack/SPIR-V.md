kSPIR-V refers to two related innovations:
* A bytecode IR/IL format to interface with different machines. Historically  GPU vendors have were flexibie in implementing compilers that turn front-end code (e.g., GLSL) into native code. Meaning that different GPUs had shader bugs or failed to compile with the same shader code.
  
  The SPIR-V bytecode is less complex and more straight-forward than front-end languages,  making inconsistent behavior less lively. 
* A toolchain where Khronos provides a vendor-independent compiler to turn GLSL (or other language front-ends) into SPIR-V blobs and vendors provide vendor-dependent compiler back-ends.
  
  The Khronos GLSL compiler verifies that the shader code is fully standards complient and produces one SPIR-V binary that can be shipped with the application program.

The SPIR-V toolchain is similar in spirit to the LLVM toolchain (SPIR-V was initially a fork of LLVM) - it separates the responsibility b/w language front-ends (GLSL, HLSL, MSL, OpenCL) and hardware back-ends. 

The difference is that graphics APIs must be appended with API ingestion calls for SPIR-V. This can be done at runtime, but more commonly(e.g., `glslangValidator` by Khronos or `glslc` by Google).

![[spir-v-ecosystem.png]]

# TODO
* https://github.com/KhronosGroup/Vulkan-Guide/blob/main/chapters/what_is_spirv.adoc

# Resources
* https://vulkan.lunarg.com/doc/view/1.2.162.1/windows/spirv_toolchain.html
* https://www.khronos.org/spir/
* https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Shader_modules
* 