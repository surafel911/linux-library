# Background

Userspace applications typically make requests to the kernel by means of system calls, whose code lies in the kernel layer. A system call usually takes the form of a "system call vector", in which the desired system call is indicated with an index number. For instance, `exit()` might be system call number 1, and `write()` number 4.

kernel is designed to be extensible, and may accept an extra module called a  device driver which runs in kernel space and can directly address the device. 

# Introduction

An `ioctl` interface is a single system call by which userspace may communicate with device drivers. Requests on a device driver are vectored with respect to this `ioctl` system call, typically by a handle to the device and a request number. The basic kernel can thus allow the userspace to access a device driver without knowing anything about the facilities supported by the device, and without needing an unmanageably large collection of system calls.

_ioctl_ is a system call, ergo it is the responsibility of any operating system running on it to implement the system call.

An `ioctl`, which means "input-output control" is a kind of device-specific system call. There are only a few system calls in Linux (300-400), which are not enough to express all the unique functions devices may have. So a driver can define an _ioctl_ which allows a userspace application to send it orders, e.g. a printer that has configuration options to check and set the font family, font size etc.

```c
int ioctl(int fd, int request, ...)
```
1.  `fd` is file descriptor, the one returned by `open`;
2.  `request` is request code. e.g `GETFONT` will get the current font from the printer, `SETFONT` will set the font on the printer;
3.  the third argument is `void *`. Depending on the second argument, the third may or may not be present, e.g. if the second argument is `SETFONT`, the third argument can be the font name such as `"Arial"`

An alternative (and possibly simplier to use) alternative to _ioctl_ is _sysctl_