# Overview

EGL is an interface between Khronos rendering APIs (such as OpenGL, OpenGL ES or OpenVG) and the underlying native platform windowing system. EGL handles graphics context management, surface/buffer binding, rendering synchronization, and enables "high-performance, accelerated, mixed-mode 2D and 3D rendering using other Khronos APIs."[2] EGL is managed by the non-profit technology consortium Khronos Group. 

EGL is an open, royalty-free standard that defines a portable interface to underlying operating system and display platforms to handle graphics context management, surface and buffer binding, and rendering synchronization. EGL also provides interop capability to enable efficient transfer of data and events between Khronos APIs.


# EGL Streams

Nvidia's DRM KMS implementation leaves much to be desired. Nvidia's DRM KMS implementation does not support GBM buffer allocations and submissions. Instead, EGL Streams are utilized to pass BOs between DRM KMS and the window system.

Rendering to using EGL relies on using a series of steps unique to Nvidia (sucks):
- [EGLDevice](https://docs.nvidia.com/jetson/archives/r35.2.1/DeveloperGuide/text/SD/Graphics/Egldevice.html)
- [EGLStream](https://docs.nvidia.com/drive/drive_os_5.1.6.1L/nvvib_docs/index.html#page/DRIVE_OS_Linux_SDK_Development_Guide/Graphics/graphics_eglstream_user_guide.html)

> EGL is a Khronos defined API consisting of a core specification and optional extensions that provides an interface between APIs, as well as a connection to the window system or other underlying platform. It provides mechanisms for creating surfaces that client APIs can read and write, importing and exporting resources created by clients, and mapping window system resources into clients. This allows it to serve as a centralized interop hub to exchange shared resources, removing the need for specialized interops between individual clients.
> 
> EGLStream is an EGL API extension that provides a mechanism that efficiently transfers a sequence of image frames from one API to another. EGLStream is supported on the following NVIDIA supported APIs: OpenGL ES, CUDA, and NvMedia, allowing frames to be shared between these APIs. 
# Resources
* [NVIDIA Direct Rendering Manager Kernel Modesetting (DRM KMS)](https://download.nvidia.com/XFree86/Linux-x86_64/396.51/README/kms.html) 
* [EGL Interoperability and EGLStream](https://docs.nvidia.com/drive/drive-os-5.2.0.0L/drive-os/index.html#page/DRIVE_OS_Linux_SDK_Development_Guide/eglstream_top.html)
* [EGLDevice](https://docs.nvidia.com/drive/drive_os_5.1.6.1L/nvvib_docs/index.html#page/DRIVE_OS_Linux_SDK_Development_Guide/Windows%20Systems/window_system_egl.html) 
* [EGLStream](https://docs.nvidia.com/drive/drive_os_5.1.6.1L/nvvib_docs/index.html#page/DRIVE_OS_Linux_SDK_Development_Guide/Graphics/graphics_eglstream_user_guide.html)

# TODO
- https://directx.com/2014/06/egl-understanding-eglchooseconfig-then-ignoring-it/
- https://jan.newmarch.name/Wayland/EGL/
- https://registry.khronos.org/EGL/sdk/docs/man/html/eglIntro.xhtml
- https://www.khronos.org/egl
- https://blog.devgenius.io/understanding-android-egl-embedded-system-graphics-library-1772d6d647e