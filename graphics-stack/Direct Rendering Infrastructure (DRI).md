An architecture that allows X clients to talk to the graphics hardware directly.
![[linux-grahics-stack-dri-stack.png]]


https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure
https://blog.mecheye.net/2016/01/dri/
https://dri.freedesktop.org/wiki/GettingStarted/
https://www.kernel.org/doc/html/latest/gpu/introduction.html
https://dri.sourceforge.net/doc/drm_low_level.html
https://www.x.org/releases/current/doc/dri2proto/dri2proto.txt
https://www.cs.nmsu.edu/~joshagam/archive/cs574/3-dri.html
https://dri.sourceforge.net/doc/design_low_level.html