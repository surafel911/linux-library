A set of interfaces and supporting libraries for writing graphics drivers in a largely device and operating system agnostic fashion by abstracting state tracking from the graphics hardware driver

# Differences from Classic Graphics Drivers

// FIXME: This section is terrible fucking fix this

The benefits of Gallium over the 'classic' approach to graphics support is that Gallium allows for user-space graphics libraries like OpenGL to target Gallium itself rather than be written specifically to the architecture of the underlying hardware. This allows for the OpenGL (and other graphics library) implementation to be decoupled from the underlying hardware (and even operating system) while still providing high performance.

In theory, these benefits will result in improved and better-maintained graphics stacks which in turn will benefit Linux users. 

Gallium3D provides a unified API exposing standard hardware functions (e.g., shader units) on modern graphics hardware. Thus, 3D graphics API implementations need only a single back-end, the Gallium3D state tracker.

In contrast, the classic-style DRI device drivers require different back-ends for each hardware platform and graphics APIs, or need translation to OpenGL at the expense of code duplication. Proprietary drivers are implemented as classic-style DRI device drivers.

![[gallium3d-diagram.png]]

# Software Architecture
Gallium3D eases graphics driver programming by splitting the driver into three parts. This is done by introducing two interfaces: *Gallium3D State Tracker Interface* and the *Gallium3D WinSys Interface*

## Gallium3D State Tracker
Each graphics API that is address by the driver has it's own [[State Tracker]], e.g. there is a Gallium3D State Tracker for OpenGL

## Gallium3D Hardware Driver
This is the actual driver code for the graphics hardware, but only as far as the Gallium3D WinSys Interface allows. There is a unique Gallium3D hardware driver for each graphics chip and only understands the [[Tungsten Graphics Shader Infrastructure (TGSI)]], an intermediate language for describing shaders. This code translated shaders translated from GLSL into TGSI further into instruction set implemented by the GPU.

## Gallium3D WinSys
This is specific to the underlying kernel of the operating system and each one implements the Gallium3D WinSys Interface to interface with all available Gallium3D hardware device drivers.


# Resources
- https://en.wikipedia.org/wiki/Mesa_(computer_graphics)#Gallium3D

# TODO:
- https://www.freedesktop.org/wiki/Software/gallium/GAOnlineWorkshop/