# Introduction

The goal was to design a hardware-independent API to give user space software access to the framebuffer to the terminal/TTY application. In the transition to desktop environments, FBDEV proved limiting and was replaced with X11.

fbdev is very limited: no configurable pipeline, no synchronization to change frame buffer concurrently, no hot-plugging, and only using preallocated buffers are among some of the issues. However, FBDEV is still currently used by the framebuffer console (fbcon) for the on-display TTY console (CNTRL+Alt+F\[1-9\]) and is accessible through `/dev/fb0`.

To replace FBDEV, the Direct Rendering Manager (DRM) is the modern kernel subsystem for graphics.

# FBCON

The framebuffer console (FBCON) is a text console that runs on top of the framebuffer device. It has the responsibility of the any standard terminal, but outputs to some graphical output (e.g., VGA console).

# Early Graphics

**Early graphics** refers to graphics that present before the full display system has been initialized. In other words, before the _init_ process has been executed. This is useful to show signs of life, kernel and init logs for debugging, or entering passwords for drive decryption.

The **FBCON** implements a bridge between a VT/TTY and graphics. `stdin` is grabbed via an input subsystem and `stdout` is rendered and displayed via FBDEV
* Can be used for kernel logs (`console=tty1`)
* Enabled with `CONFIG_FRAMEBUFFER_CONSOLE`
* Can also display a logo: `CONFIG_LOGO`

In order to present a framebuffer and framebuffer device driver is required. The framebuffer device driver is provided by FBDEV and not KMS (this is why FBDEV is still an up-to-date subsystem). fbcon can inherit a framebuffer and display configuration from the boot software, or using the compatibility layer that implements the FBDEV API using Direct Rendering Manager drivers (the modern graphics drivers explained later). There are also dedicated FBDEV drivers for this but they are old and not suitable for modern use.


# Code Highlights

Linux Kernel:

* `drivers/video/fbdev/core/fbcon.c`:
	* `struct const fb_con`
	* ``fbcon_set_bitops()
	* `fb_con_prepare_logo`
	* `do_fbcon_takeover()`
	* `fbcon_redraw()`
	* `fbcon_putc()`
* `drivers/video/fbdev/core/bitblit.c`:
	* `bit_putcs()`
* `drivers/tty./vt/vt.c`:
	* `struct tty_operations con_ops`
	* `do_update_region()`
	* `do_take_over_console`



https://docs.kernel.org/fb/fbcon.html