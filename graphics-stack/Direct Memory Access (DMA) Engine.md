
# Buffer Sharing
https://docs.kernel.org/driver-api/dma-buf.html
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842418/Linux+DMA+From+User+Space
https://docs.kernel.org/driver-api/dma-buf.html

https://asahilinux.org/2023/03/road-to-vulkan/

# Synchronization
https://www.collabora.com/news-and-blog/blog/2022/06/09/bridging-the-synchronization-gap-on-linux/

https://lwn.net/Articles/814587/
https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2017/Nicolas%20Dufresne%20-%20Linux%20Explicit%20Fences%20in%20GStreamer.pdf
https://www.collabora.com/news-and-blog/blog/2016/09/13/mainline-explicit-fencing-part-1/
https://www.collabora.com/news-and-blog/blog/2016/10/18/mainline-explicit-fencing-part-2/
https://www.collabora.com/news-and-blog/blog/2017/01/26/mainline-explicit-fencing-part-3/
https://registry.khronos.org/EGL/extensions/ARM/EGL_ARM_implicit_external_sync.txt

https://www.youtube.com/watch?v=gZGHJOMrMP0