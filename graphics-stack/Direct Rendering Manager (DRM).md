# Background

To drive a display, there needs to be a subsystem for displaying the final framebuffer from the graphics hardware driver to the display. The historic and legacy Linux kernel subsystem and uAPIs for display is [[FBDEV]]. 


# Introduction

- The Direct Rendering Manager was introduced to interface with and drive graphics cards
	- Initialize the card, load firmware, etc.
	- Multiplex GPU command queues b/w multiple applications
	- Manage GPU memory (allocations, access, etc.)
- A complimentary component called [[Kernel Mode Setting (KMS)]] is a complimentary component 
	- Intended to correct race conditions in previous implementations b/w rendering (kernel-space) and mode-setting (user-space)
	- KMS provides a way to configure the display pipeline of a graphics card

Each discrete graphics device (e.g., GPU, display controller) has its own DRM character-device.

- DRM in kernel space: providing-low level applications access to hardware features
	- **KMS**: covers display
	- **KMS atomic**: extension for atomic state changes; groups changes to display hardware together
	- **GEM**: memory management, cache synchronization, etc.
		- Zero-copy memory, import existing memory to DRM uAPIs (**PRIME**)
		- Fences: explicit synchronization points b/w graphics devices (**Syncobj**)
	- **Render**
		- No unified API b/w drivers - base building block on kernel side, and one API per driver on user space side (since it targets one graphics device
	- Low-level user space libraries to interface with DRM:
		- **libdrm**:  wrapper for DRM system calls (i.e., ioctl calls) to interact with DRM components
- DRM in user space
	- Low-level display protocols APIs
		- [[X11]]: legacy protocol
		- [[Wayland]]: modern protocol, various extensions
		- A way for applications to coordinate submitting framebuffers and getting input events from a central location (i.e., display server or display protocol)
	- Implementations
		- [[Mesa 3D]]: Vulkan and OpenGL (through [[Zink]]) implementations; is framework and itself a DRM drivers (more specifically a repository with all DRM drivers)
	
- DRM and KMS together:
	- Exposes each display pipeline element separately for configuration. Permits flexible configurations, particularly for micro-controllers
	- Generic uAPI - any user space DRM implementation can work with any DRM driver
	- Property-based system for configuring different parts of the pipeline separately
	- Dynamic user space framebuffer allocation (great for synchronization)
	- Atomic API for grouping and synchronizing changes to the display hardware, useful for configuring multiple monitors simultaneously state or presenting multiple framebuffers/planes on the same display, all without an intermediate state
	- Legacy compatibility layer with fbdev (DRM registers itself as an fbdev  driver, no need for legacy implementations)

# DRM Stack

![[direct-rendering-management-diagram-1.png]]
	* **Note that GEM/TTM are user space frameworks** 

## User Space
1. X11 Plugin is a stand in for any [[X|X11]] or [[Wayland]] compositor.
2. `libdrm` is the user space library for DRM / KMS driver
3. [[Mesa 3D]] is the user space driver for graphics hardware
4. [[Graphics Execution Manager (GEM)|GEM/TTM]] are user space memory managers for allocating memory for the user-space DRM drivers (e.g., framebuffers, textures, etc.)
5. PRIME is the cross device buffer sharing framework in DRM, i.e., exporting/importing buffers among devices in DRM
	1. E.g., hybrid graphics in laptops where rendering is done on a dedicated graphics card and sending the results to the card which has the display connected (called GPU offloading)

## Kernel Space
1. KMS is the API within DRM that the user space driver/compositor to interface with graphics hardware
2. GEM/TTM utilize the Contiguous Memory Allocator (CMA) within the kernel to  allocate large chunks of RAM/VRAM with contiguous physical addresses.
	1. Buffer is used in DRM driver
3. `dma-buf` is the kernel framework for sharing buffers for hardware DMA across multiple device drivers and subsystems
	1. Implemented by sharing file descriptors across Unix sockets
4. Mesa _is_ DRM driver for `libdrm`

## Hardware
1. Display engine that is in the SoC or graphics card. Abstracted by CRTC
2. RAM/VRAM is allocated and managed by GEM/TTM in user space
3. The GPU is directly controlled by the DRM driver. Mesa is the DRM driver

![[direct-rendering-management-diagram-2.png]]

# Software Architecture
## Direct Rendering Infrastructure
- The synchronization and infrastructure for the graphics application, kernel, and the display server is called the [[Direct Rendering Infrastructure (DRI)]] 
	- DRI refers to both the infrastructure and the library.
- Rendering (DRM) and mode-setting (KMS) are under one unified DRM API
- When the Linux kernel detects a graphics-card on the machine, it loads the correct 
- There can only be one **DRM master** client which is allowed to configure the display (KMS)

- The Direct Rendering Manager resides in both user-space and kernel-space
	- The kernel-space component of DRM handles hardware communication
	- The user-space component of DRM handles the implementation of standard graphics APIs
- GPUs detected by DRM (refereed to as a _DRM device_) are exposed as a device file in the `/dev` and are interfaced through the device file.
	- User-space programs must open this file and use ioctl calls to communicate with DRM
-  The DRM software design consists of three parts: DRM library, DRM core, and DRM driver


## DRM Library
- The DRM library (called _libdrm_) is the user-space library for accessing the DRM.
	- Merely a wrapper that provides C functions for every ioctl of the DRM API, as well as constants, structures, and other helpful elements.
	- Good for reuse and code sharing between applications
	- Avoids exposing kernel interface directly to applications
- _libdrm_ is a low-level library used by Mesa drivers, compositors, and similar applications as part of the DRM core interface (explained later)
	- _libdrm_ contains all non-card specific, hardware independent code for memory and buffer interfaces, context handling, etc.
	- A DRM driver (explained later) can extend and enhance the libdrm API with an extra _libdrm-driver_ that can be used by user-space to interface with additional ioctls.

- When running an OpenGL application (e.g., `glxgears`), it loads Mesa, which itself loads _libdrm_
- There are situations where GEM/TTM are not enough, so Mesa loads hardware-specific _libdrm_ drivers. 
	- Looking `ls /usr/lib64/libdrm_*` shows there are hardware-specific drivers.

## DRM Core
- The DRM core provide a basic and generic framework for common, hardware-independent functionality.
	- DRM core implements the `DRM_CLIENT_CAP*` flags in `xf86drm.h` that exposes hardware-independent capabilities.
	- Multi-GPU is supported by sharing the DRM core with multiple DRM drivers
- Internally DRM core registration of devices (e.g., sysfs and class code), preventing devices from interfering with each other, handling of shared resources (e.g., memory), etc.
	- Runs in kernel-space

## DRM Driver
- The DRM driver implements the hardware-specific part of the DRM API and is specific to the type of GPU it supports
	- Provides implementation of the ioctls not implemented by DRM core, and also extends the API with a _libdrm-driver_ library.
	- Runs in user-space

the current driver files converted to the new
interfaces, I don't mind retaining some of the templating work, I like the
fact that we don't have 20 implementations of the drm probe or PCI tables
or anything like that, so I think some small uses of DRM() may still be
acceptable from a maintenance point of view.

## Kernel
- When the Linux kernel loads and detects a graphics card on the machine, it loads the correct device driver (located in kernel-tree at `./drivers/gpu/drm/\*`) provides character-devices to control it
  
  TODO: 
	  https://dri.freedesktop.org/docs/drm/gpu/drm-uapi.html#render-nodes
	  https://dvdhrm.wordpress.com/2013/09/01/splitting-drm-and-kms-device-nodes/
	  
	- Primary nodes at `/dev/dri/card0` and `/dev/dri/controlID64` are used for KMS (mode) operations
	- Rendering nodes at  `/dev/dri/renderD128` are used for render options with a driver-specific interface
- Unlike DRM KMS, the kernel has no generic uAPI for rendering; each render driver in DRM has it's own **driver-specific kernel API**, with support with thin helpers in _libdrm_
	- GPUs are highly specific and is impossible to create a generic interface b/w kernel and user space
- Handles various low-level aspects:
	- **Memory buffer**, i.e. buffer object (BO) management from GEM or TTM
	- **Command stream** validation from user space and submission to GPU
	- **Task scheduling** with the DRM scheduler
- Most of the heavy lifting is left to user space (primarily I/O in kernel)
- Proprietary implementations use their custom interfaces as kernel modules

## Memory Management
- Memory buffer objects (BOs) used in _libdrm_ come from user space memory management interfaces: TTM and GEM are two competing interfaces that service this purpose
	- TTM was the first DRM memory manager to be developed and tried to be a one-size-fits-them all solution.
		- Provides a single, gigantic uAPI to accommodate the needs of all hardware, supporting devices with both (UMA (i.e., integrated graphics) and VRAM (i.e. most discrete graphics) devices. 
			- E.g., TTM's fencing mechanism - synchronization of memory access b/w the GPU and CPU, similar to semaphores - has an odd interface.
		- Resulted in a large, complex piece of code that turned out to be hard to use for driver development.
	- GEM started as an Intel-sponsored project in reaction to TTM’s complexity. Instead of providing a solution to every graphics memory-related problems, GEM identified common code between drivers and created a support library to share it. GEM has simpler initialization and execution requirements than TTM, but has no video RAM management capabilities and is thus limited to UMA devices.
		- Instead of moving to GEM, DRM drivers will usually implement the needed memory management (including fencing), in TTM but provide GEM-like APIs.
	
#### Coherency
If the GPU accesses a BO when the CPU has the BO memory mapped to a device or used in a command buffer, then the BO must be flushed to memory and marked so to be coherent (i.e., consistent to) the GPU's view of memory. Likewise, if the GPU accesses a BO after the GPU has finished rendering to the BO, then the BO must be made coherent with the GPU's view of memory, usually involving GPU cache flushes.

This core CPU<-->GPU coherency management is provided by device-specific ioctl calls which evaluates a BO's current domain and performs any necessary flushing or synchronization to put the BO it the desired coherency domain.

In other words, device-specific ioctl calls in the hardware-specific DRM & kernel drivers are required to synchronize access / modification of BOs b/w the CPU and GPU.

#### Command Execution
TODO

## Submitting Pixels

Applications want to submit their pixels to the display server. [[Wayland]] is the display server in this example

Submission through transferring pixels (i.e., _memset_) is deprecated behavior.
* **Zero-copy** buffer sharing with the display server is used instead.
* Buffers are identified by API-specific identifiers, which are commonly file descriptors.

There are two main APIs for **buffer sharing**: 
* [[Shared Memory (SHM)]] is a buffer allocated by the kernel (system calls) where the application fills in pixels (CPU-rendering) and shared to display server
* [[Embedded OpenGL (EGL)]] is a buffer allocated by the graphics driver which is filled in by GPU rendering and shared with the display server; is the glue that merges the rendering and display server

Allocations are often managed by the APIs
* Zero-copy import may be possible: e.g., `EGL_EXT_image_dma_buf_import` allows EGL to import existing memory as a [[DMA-BUF]] file descriptor 
* Might cause hardware access issues (but usually works): e.g., the virtual memory is fragmented and the GPU cannot utilize fragmented memory (not an issue in well designed systems)

Applications needs **coordination** mechanisms to notify the display server that it's content has changed
* Applications specify a **damage region** (notify that an area of the surface has changed) to this display server after sharing buffers (e.g., `wl_surface_damage`)
* Applications notify the display server that it has finished and ready for presentation it's changes with a sync point  (e.g., `wl_surface_commit`)

## Compositing

A unique buffer is submitted to the display hardware. Display servers gather application framebuffers (also called surfaces) and composites them.

The compositor is aware the contents of all visible applications, and surfaces are stacked (i.e., composited) according to the window manager policy. The unique framebuffer needs to be redrawn when a visible application indicates its contents have changed.

Compositing is a very demanding task: full redraws must be avoided at all costs. Damage regions are used so that the compositor only updates what is minimally required. Hardware acceleration is necessary for their dedicated hardware components: framebuffers/surfaces as textures, hardware plans (can be used, but usually not), and a dedicated hardware plane/texture for cursors

## Page-Flipping

**Tearing** is a well-known issue with display sync: it's the product of a race condition between the framebuffer update and the display sync. The goal is achieving a glitch-free display content update.

Tearing is resolved with a **double-buffering** approach. Double-buffering uses a technique called **page flipping** to prevent tearing. A front buffer is shown, while the back buffer is being rendered to. The roles of the two buffers are exchanged (page flipped) at the next vertical sync (**vblank**) point. More buffers can be used (triple-buffering) but that introduces latency

DRM KMS ensures that  the page flip occurs at vblank.
* Scheduled using `DRM_IOCTL_MODE_PAGE_FLIP` (with target) but this is considered legacy
* Scheduled with atomic commit using `DRM_IOCTL_MODE_ATOMIC`
* Can notify user space when (blocking or async event) when the page flip occurs

## Universal Planes

In 2014 Matt Roper (Intel) developed the _universal planes_ (or _unified planes_) concept by which framebuffers (_primary planes_), overlays (_secondary planes_) and cursors (_cursor planes_) are all treated as a single type of object with an unified API. Universal planes support provides a more consistent DRM API with fewer, more generic ioctls. In order to maintain the API backwards compatible, the feature is exposed by DRM core as an additional capability that a DRM driver can provide.

## DRM Leasing

https://www.x.org/wiki/Events/XDC2017/packard_drm_lease.pdf
https://drewdevault.com/2019/08/09/DRM-leasing-and-VR-for-Wayland.html

# Examples
Source code at
https://github.com/torvalds/linux/tree/master/drivers/gpu/drm

Driver and client capabilities defined in Linux’s
https://elixir.bootlin.com/linux/latest/source/include/uapi/drm/drm.h

A reference implementation for a DRM render driver
https://github.com/torvalds/linux/tree/master/drivers/gpu/drm/v3d

Entry point:
https://github.com/torvalds/linux/blob/master/drivers/gpu/drm/v3d/v3d_drv.c

Dedicated documentation:  
https://dri.freedesktop.org/docs/drm/gpu/v3d.html




# TODO
VT Mode, VT Switching, and Code Highlights
https://www.youtube.com/watch?v=lEUCgGLZYo8
https://youtu.be/wjAJmqwg47k?t=1758



* https://moi.vonos.net/linux/drm-and-kms/
* https://elinux.org/images/2/27/Szyprowski.pdf
* 
* https://wiki.st.com/stm32mpu/wiki/DRM_KMS_overview#modetest_-28DRM-2FKMS_test_tool-29
* https://lists.freedesktop.org/archives/dri-devel/2014-March/055222.html
* https://events.static.linuxfound.org/sites/events/files/slides/brezillon-drm-kms.pdf
* https://dri.freedesktop.org/wiki/DRM/
* https://bootlin.com/doc/training/graphics/graphics-slides.pdf
* https://en.wikipedia.org/wiki/Direct_Rendering_Manager
* https://landley.net/kdocs/htmldocs/drm.html
* https://eleni.mutantstargoat.com/hikiko/wp-content/uploads/2017/11/index-1.pdf
	* https://www.youtube.com/watch?v=axucgdUziYQ
	* https://papers.freebsd.org/2019/bsdcan/vadot-adventure_in_drmland/

# Resources
- https://www.kernel.org/doc/html/v4.17/gpu/drm-kms.html# 
- https://www.kernel.org/doc/html/v4.15/gpu/drm-mm.html#
- https://www.youtube.com/watch?v=wjAJmqwg47k
- https://www.youtube.com/watch?v=LbDOCJcDRoo
- https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ff72145badb834e8051719ea66e024784d000cb4
- [Libdrm-dev/DRM-Memory Manpages](https://manpages.debian.org/testing/libdrm-dev/drm-memory.7.en.html) 
- [Getting pixels on screen on Linux: introduction to Kernel Mode Setting - Simon Ser](https://youtu.be/haes4_Xnc5Q)
- [Direct Rendering Manager](https://en.wikipedia.org/wiki/Direct_Rendering_Manager)
- [An Introduction to the Linux Graphics Stack - Isabella Basso](https://crosscat.me/an-introduction-to-the-linux-graphics-stack/)
- [Direct Rendering Manager](https://en.wikipedia.org/wiki/Direct_Rendering_Manager#Software_architecture)
	- [New Proposed DRM interface design](https://lwn.net/Articles/100839/)
- [Graphics: A Frame's Journey - Daniel Stone, Collabora](https://www.youtube.com/watch?v=nau2dgdXWOk)
