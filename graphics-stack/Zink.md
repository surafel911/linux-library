
[[Zink]] is an [OpenGL](https://www.opengl.org/) implementation on top of [Vulkan](https://www.khronos.org/vulkan/). Or to be a bit more specific, Zink is a [Mesa](https://www.mesa3d.org/) Gallium driver that leverages the existing OpenGL implementation in Mesa to provide hardware accelerated OpenGL when only a Vulkan driver is available.

# TODO

* https://www.collabora.com/news-and-blog/blog/2018/10/31/introducing-zink-opengl-implementation-vulkan/
* https://www.phoronix.com/review/zink-mesa-221
* https://blogs.gnome.org/uraeus/2022/04/07/why-is-kopper-and-zink-important-aka-the-future-of-opengl/
* https://www.youtube.com/watch?v=RhV72Xc8I1A
* https://www.youtube.com/watch?v=YlS7tCwW_9U