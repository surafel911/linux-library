Generic Buffer Management (GBM) is an API that provides a mechanism for allocating buffers for graphics rendering tied to Mesa. GBM is intended to be used as a native platform for EGL on DRM or openwfd. The handle it creates can be used to initialize EGL and to create render target buffers.[115]

Mesa GBM is an abstraction of the graphics driver specific buffer management APIs (for instance the various libdrm_* libraries), implemented internally by calling into the Mesa GPU drivers.

For example, the Wayland compositor Weston does its rendering using OpenGL ES 2, which it initializes by calling EGL. Since the server runs on the "bare KMS driver", it uses the EGL DRM platform, which could really be called the GBM platform, since it relies on the Mesa GBM interface.


https://zamundaaa.github.io/wayland/2023/03/10/porting-away-from-gbm-surface.html