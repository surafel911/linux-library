# History

## Early Graphics - FBDEV

Graphics before the introduction of desktop environments was simply for showing a terminal/TTY on a computer console (think Unix days). This was implemented in a kernel subsystem called [[FBDEV]]. 

The goal was to design a hardware-independent API to give user space software access to the framebuffer to the terminal/TTY application. In the transition to desktop environments, FBDEV proved limiting and was replaced with X11.

## 2D Graphics
In the early days, applications would do all drawing indirectly through the X server using
Xlib/XCB.

X server was responsible for handling the graphics hardware by design; drivers were integral to the architecture.

User space drivers called [[Driver Dependent X (DDX)]] drivers implemented 2D operations in the X server and critical for legacy graphics stack. There would be `xorg` or `xf86` (an hardware-agnostic standard) packages that would implement drivers for the X server.

![[linux-graphics-stack-legacy-ddx.png]]

## 3D Graphics

Developers wanted to use OpenGL to take advantage of new 3D hardware acceleration in `libGL.so`. However, the X server had sole access to graphics hardware and prevented `libGL.so` to interface with the hardware. The solution was *Indirect Rendering*, sending OpenGL calls to the X server through an X11 protocol extension and have the X server implement the OpenGL calls. Very ineffective for gaming.

## Enter Direct Rendering Infrastructure

[[Direct Rendering Infrastructure]] is the new architecture that allows X clients to talk with the graphics hardware directly. Implementing DRI required changes to the X server and graphics stack at large.

Note that DRI usually refers to the complete architecture, but can also refer to the specific interaction of applications and the X server. Be careful when researching.

Another important part of DRI is the [[Direct Rendering Manager]]; the kernel side of the DRI architecture. Here DRM handles sensitive aspects such as hardware locking, synchronization, and hardware access. DRM also provides user space with an API to submit commands and data in a modern format; effectively allowing user space to communicate with graphics hardware.

In DRI, there are still separate DRM drivers for each GPU.

![[linux-grahics-stack-dri-stack.png]]

Note that it is possible to create graphics drivers that do not follow the DRI architecture in Linux. For example, the NVIDIA proprietary driver installs a Kernel module that implements similar functionality to DRM but with a different API that has been designed by NVIDIA, and obviously, their corresponding user space drivers (DDX and OpenGL) will use this API instead of DRM to communicate with the NVIDIA kernel space driver.

## Enter Mesa
[Mesa] is a free software implementation of the OpenGL specification; it provides `libGL.so` and can provide accelerated 3D graphics by taking advantage of DRI architecture to gain direct access to the graphics hardware.

Mesa's [GLX] (the extension of the OpenGL spec that addresses the X11 platform) implementation uses the DRI implementation to synchronize between Mesa rendering to the surface (i.e., X window) and the X server owning the window *and* displaying it's content is done with.

Mesa's graphics API implementation (i.e., allocating buffers, running draw calls, etc.) is enabled by the DRM driver which allows user space programs to access hardware. In other words, Mesa's OpenGL implementation require DRM API to use the target hardware


# Linux Graphics

For simplicity's sake we are bisecting the discussion of the Linux graphics stack into two sections:
1. Driver Stack - The software stack responsible for driving hardware components such as the graphics hardware and the display 
2. Display Stack - The software stack responsible for multiplexing the system's display/input devices between clients
# Driver Stack

![[linux-graphics-stack-flowchart.png]]

The following sections correspond to this linear representation of the stack. Only novel entries are expanded upon
* Application
* Engine
* Graphics API
* Compiler
* Driver
* Kernel
* Hardware

Mesa is responsible for the Vulkan/OpenGL implementation drivers, compiler stack, Gallium-implementation, and IR

## Graphics API
Graphics APIs are implemented in graphics hardware drivers. Goals are:
* Compile shader programs
* Bind shaders, command buffers, and data
* Executes draw calls

### [[OpenGL]]

### [[Vulkan]]
* Lower CPU overhead
* Reduce complexity in user space driver by removing state in OpenGL and providing an interface for driver constructs
	* Driver responsibilities become engine responsibilities
* GLSL parser eliminated, [[SPIR-V]] blobs instead

## Compiler
Very similar to CPU program compilation stack, except for the graphics hardware:
1.  Parse GLSL shader into AST
	* In Vulkan the compiler is removed from the driver
1. Translate AST to a neutral intermediate representation, e.g. ([[NIR]]) or [[LLVM]]
	* Apply common optimizations to IR
	* Goal is to share hardware-agnostic graphics code
2. Translate optimized IR to a hardware-specific IR
	* Apply hardware-specific optimizations to IR 
3. Translate hardware-specific IR to final hardware binary

There are many different compilers and layers to the compilation stack for shaders. There is a unique issue in graphics drivers in trying to make a common interface (i.e., have as much code sharing as possible) on different types of graphics hardware (i.e., integrated GPU vs discrete GPU, 10 year old graphics card vs most recent graphics cards). This results in tension between hardware-agnostic and hardware-specific code.

The solution is to have turtles all the way down (i.e., never ending dependencies). Have different IRs for different purposes:
* [[GLSL IR]] (tree-based IR) for GLSL code that can be shared across graphics hardware
* NIR (flat IR) for common shader code that is easy to optimize ([[NIR SSA]])
* Various hardware IRs to apply hardware-specific optimizations
This composition of IRs that chain together permit a composable compiler stack that can abstract dozens of graphics hardware instructions to a few graphics APIs

In this example, the Panfrost driver is abstracted entirely from any higher level IRs or programs, and only takes in NIR as input to translate into hardware-specific assembly for the hardware. 

![[linux-graphics-stack-compiler-example.png]]

This stack makes Mesa drivers simpler and reduces driver development costs compared to proprietary driver. However, the Mesa compiler stack is still fairly complex

![[linux-graphics-stack-mesa-compilers.png]]

## Driver
The graphics drivers Implementation of the graphics APIs. Graphics drivers have three main responsibilities:
1.  Compile shaders (i.e., compiler binaries & toolchains)
2. Translate "program/constant state objects" to hardware state 
	* E.g., commands, registers, descriptors
	* Bind hardware state(s) and emit draw calls
	* Make a system call with the GPU memory address of the command buffer
3. Manage memory
	* Allocate GPU memory for buffers and commands
	* Memory management is mostly handled by user space applications

### Direct Rendering Manager

The Linux kernel's GPU and display subsystem  is called the [[Direct Rendering Manager (DRM) | Direct Rendering Manager (DRM)]]. DRM is the over-arching display driver framework for driving GPU's and using graphics APIs.

The [[Kernel Mode Setting (KMS)| Kernel Mode Setting (KMS)]] is a complimentary subsystem for controlling the display. KMS is the final step in the display pipeline, yet it has nothing to do with rendering. KMS is used to drive the display to turn pixels into light.

Graphics hardware is complex, therefore drivers for the hardware are complex. There needs to be separation of the kernel-space and user-space driver. Not all of the code needs to run in a privileged context, and debugging in user- space is much easier

### Kernel-Space Drivers

The kernel-space drivers deal with:
* Memory management (allocating buffers, framebuffers,  passing bufferts, etc.)
* Command stream submission /scheduling (multiplexing GPU among several processes)
* Handling interrupts and signaling (e.g., when GPU signals it has finished executing the command stream)

Kernel driver interfaces with open-source user-space drivers are in the Linux tree: `drivers/gpu/drm`.

### User-Space Drivers

The user-space drivers are more complex than the kernel-space driver.  It's responsible to implement  the graphics APIs:
- Maintaining the API specific state machine (i.e.,  [[Gallium3D]] if any)
- Managing off-screen rendering contexts (if any)
- Compiling shaders into hardware-specific bytecode
- Creating, populating, and submitting command streams to kernel
- Implement synchronizations with the kernel-space drivers

The user-space driver is also responsible for providing an interface for the [[Display Server]] to interface with it. All composition is done with the user-space driver:
- Managing on-screen rendering contexts
- Binding/unbinding render buffers
- Synchronizing on render operations

### [[Mesa 3D]]
Is an umbrella for user-space graphics drivers. Any graphics vendor worth anything has code in the mesa driver:
* OpenGL/Vulkan and GLSL compiler implementations
* Common [[Gallium3D]]  infrastructure
* Common [[NIR]] infrastructure

A bulk of GPU drivers run in user-space, not kernel-space 
* Reduces the number of system calls
* More secure by having a smaller attack surface in kernel space

The two supported graphics APIs: OpenGL and Vulkan, have two different implementation approaches in Mesa.

OpenGL
- Since OpenGL has behaviors that can be abstracted from the hardware implementation (e.g., the internal state machine), Mesa provides a front-end for OpenGL APIs (i.e., [[libGL|libGL(ES)]])
- The OpenGL hardware drivers implement the DRI driver interface
- The specific OpenGL hardware drivers needed are shared libraries. The right driver to use is determined and loaded on-demand at runtime.
- For OpenGL, Mesa utilizes [[Gallium3D]] interfaces to abstract hardware components into software constructs, and abstracts the OpenGL state machine.
	- This allows an OpenGL hardware driver to target Gallium3D rather than implement the state tracker themselves

Vulkan:
- Khronos has its own driver loader (open source)
- Mesa just provides Vulkan drivers
- No abstraction for Vulkan drivers (e.g., no use of Gallium3D; no state machine in Vulkan),
- Code sharing for generic behaviors (e.g., factorization) through libraries

### GPUs: OpenGL Vulkan + EGL

OpenGL, OpenGL ES, and Vulkan are  APIs for accelerated 3D rendering. All of these APIs rely on EGL and Vulkan Window System Integration (WSI) to bridge rendering and the display

GBM bridges EGL and the Vulkan WSI to KMS as a side channel
## Kernel
* Program the hardware MMU to map GPU memory
	* It is the kernel's responsibility to interface with the hardware securely, not the driver
	* It is the kernel's responsibility to handle multiplexing the GPU among programs, similar to a CPU (e.g., graphics cards may be able to run multiple different *types* of shaders concurrently)
* Program the hardware to start executing the job in graphics memory

The kernel simply serves as an interface to the graphics hardware through system calls. It is not responsible for driving the graphics hardware directly.

## Hardware
* Read the state stored in memory
* Load the command buffer to instruction cache
* Spawn threads  


# Display Stack

## Overview

The total Linux graphics stack has two concurrent stages; rendering and presentation. The graphics stack is responsible for providing user space with APIs to communicate with graphics hardware (e.g., OpenGL, Vulkan, OpenCL).  The goals of the display stack is to present the rendered output from the graphics stack onto a display.

There are three overall steps to presentation on the display stack:
1. Applications draw pixels into pixel buffers
	* OpenGL and Vulkan have extensions which allow them to render to pixel buffers owned by the display server
2. Display must be configured for the pixel buffer
	* E.g., what resolution, refresh rate, color-depth, etc. should the display run at
3. Pixel buffer must be sent to display controller
	* The display controller is responsible for driving the display given a configuration and the pixel buffer.

Here is a basic view of the display stack. On the top of the stack we have the application, [modetest](https://wiki.st.com/stm32mpu/wiki/DRM_KMS_overview#modetest_-28DRM-2FKMS_test_tool-29). `modetest` is an application for listing all display capabilities: connectors (e.g., HDMI, DP, etc.) and display modes (e.g., resolutions, refresh rates, etc.). 


![[linux-graphics-stack-simple-drm-stack.png]]

`modetest` gets this information from `libdrm`; a user space, device-agnostic interface for the DRM driver.

Next is the DRM block, which is where the [[Kernel Mode Setting (KMS)]] interface sits. As the name suggests, KMS is part of the DRM driver that sets the display configuration (i.e., the display mode) to the display controller.

Part of the DRM block is the actual DRM driver (this example uses `rockchipdrm` ). The DRM driver is a platform-specific implementation of DRM to drive the display controller and graphics implementation (i.e., Mesa is often the DRM driver).

For to the [[Direct Rendering Manager (DRM)|DRM]]  and [[Kernel Mode Setting (KMS)|KMS]] pages for more information about the pipeline.

![[linux-graphics-stack-diagram.png]]

![[linux-graphics-stack-drm-kms-stack.png]]

Here is a diagram of the GPU stack that incorporates KMS, DRM, and OpenGL ES. On the left hand side is the KMS stack in the DRM kernel implementation in `libdrm`. KMS has direct access to the display.

On the right hand side is the graphics stack, the application uses Mesa to interface with both the GPU hardware and the display. To present rendered content to the display, Mesa uses the Generic Buffer Management (GBM) API to allocate framebuffers that are sent to KMS (often by sending file descriptors through `ioctl` calls.

Mesa couples the GBM API with the OpenGL/OpenGL ES through the [[Direct Rendering Infrastructure (DRI)|DRI]] (i.e., `libdri`) and DRM (i.e., `libdrm`) implementations. OpenGL / OpenGL ES are implemented by the panfrost DRM driver, which drives the graphics hardware

Conventional applications do not directly interface with Mesa, DRM, or KMS, nor do they directly interface with input libraries such as `evdev` or `udev`. Rather, applications depend on a display server to abstract and multiplex these interfaces among them, often called clients to the display server.

## Window System

The window system is part of the display stack, and they multiplex the system display/input devices between one or more clients. Window systems are implemented via protocols in a client-server architecture.

There are two predominant display server protocols, X11 and [[Wayland]]. They are similar in purpose, so X11 is omitted since it is being replaced by Wayland. Here is a diagram of the Wayland protocol and how it fits in with Mesa, DRM, and KMS.

Clients provide images (i.e., framebuffers) and context to the display server, also known as the compositor, to display the client's contents. The compositor provides input-device events, hints to the client about how to display (e.g., timings), etc.

![[wayland-graphics-stack.png]]

Weston is the reference display server for Wayland, otherwise known as the Wayland compositor. Wayland clients are applications, widgets, etc that submit their framebuffers to Wayland for composition.

The Wayland compositor is directly responsible for mode setting; consider how the compositor is able to change the display settings for the desktop environment.

The edges labeled "Wayland" do not refer to the actual socket-based protocol, rather show how Wayland interfaces with other components of the display / graphics stack. Wayland uses OpenGL (in this example Weston uses `simple-egl`) to render the final framebuffer.

EGL & the Vulkan Window System Integration (WSI) bridge rendering together with the window system. A component that is not shown in this graph is GBM. GBM (the acronym does not represent anything) is a buffer management subsystem in the Linux kernel where EGL/Vulkan WSI pass buffers to each other to side-step architectural issues.

The total graphics stack is designed to increase in abstraction and intention the higher up the graph you go. the DRM and KMS subsystems and OpenGL ES and EGL are hardware-specific and very direct. The compositors have no hardware-specific code and is the first layer where policies and behaviors of what makes a good desktop are implemented.

![[Pasted image 20230822220912.png]]

\*\* **_Make sure this is true_**
Instead of using the kernel DRM implementation directly, Wayland uses the DRI interface (`libdri` ) to interface with DRM drivers (e.g., Mesa drivers).

# Boot

During boot, [[FBDEV|FBCON]] binds to the framebuffer in [[FBDEV]] as soon as it is available. Often it shows a text console to dump kernel logs as the system is starting.

The kernel spawns multiple VTs/TTYs at boot. Only a single VT can be active at a given time (`tty1` at boot). You can switch them using Ctrl + Alt + F1-9

## Bootsplash
Users expect a waiting screen rather than kernel logs from [[FBDEV]] when booting the system. This waiting screen is not a kernel-level feature; there are dedicated applications for the task that run after the **init** process as root. Typically this is implemented in [initramfs](https://wiki.gentoo.org/wiki/Custom_Initramfs). 

Bootsplash applications can either use FBDEV or DRM KMS directly. The various implementations are:
* **Plymouth**: most advanced (used by Steam Deck); progress, animations, supports DRM KMS or FBDEV
* **Psplash**: from Yocto Project, progress, uses FBDEV
* **Fbsplash**: themeable, progress, uses FBDEV

## VT Mode

As soon as user space starts it wants to bind to the framebuffer that FBCON is currently occupying. This requires a coordination mechanism between FBCON and user space clients.

This is implemented through VTs. The framebuffer can be bound to multiple VTs, and applications need to be aware of which VT currently owns the framebuffer.

A mechanism called **VT sharing** provides this coordination between FBCON and user space. VT sharing ensures that access to the display pipeline is exclusive and requires privileged operations. To switch from FBCON to user space graphics, FBCON needs to be detached.

This active coordination is done with **VT modes** in the kernel to reflect the current VT state. There are two just two VT states: `KD_TEXT` where FBCON is attached to the VT, and `KD_GREAPHICS` where the framebuffer is ready for user space graphics to use. This switch is explicitly signaled with a `KDSETMODE` ioctl, using the TTY file descriptor (controlling terminal or not)

https://unix.stackexchange.com/a/704488
https://dvdhrm.wordpress.com/2013/08/24/how-vt-switching-works/

# Conclusion

This is a simplified version of both the Linux graphics and mode-setting (i.e., display) stacks

![[linux-graphics-stack-complete.png]]

# Resources
* [A tour around the world of Mesa and Linux graphics drivers](https://blogs.igalia.com/itoral/2014/07/18/a-tour-around-the-world-of-mesa-and-linux-graphics-drivers/) 
	* [A brief introduction to the Linux graphics stack](https://blogs.igalia.com/itoral/2014/07/29/a-brief-introduction-to-the-linux-graphics-stack/) 
- [The Open Graphics Stack - Alyssa Rosenzweig, Collabora](https://www.youtube.com/wach?v=e5bPfatf7Wc) 
- [Navigating the Linux Graphics Stack - Michael Tretter, Pengutronix](https://www.youtube.com/watch?v=S_rqgyId9YE)
- [Open Source Graphics 101: Getting Started - Boris Brezillon, Collabora](https://www.youtube.com/playlist?list=PLdND2EtevwMQKGDKgxwVyVEJLkpMvKwhg) 
- [Graphics: A Frame's Journey - Daniel Stone, Collabora](https://www.youtube.com/watch?v=nau2dgdXWOk)

# TODO
- [gaming on wayland](https://zamundaaa.github.io/wayland/2021/12/14/about-gaming-on-wayland.html)
* https://en.wikipedia.org/wiki/Direct_Rendering_Manager
	* https://moi.vonos.net/linux/graphics-stack/
	* https://phd.mupuf.org/files/fosdem2013_drinext_drm2.pdf
* https://elinux.org/images/7/71/Elce11_dae.pdf
* https://blogs.igalia.com/siglesias/assets/files/talks/ubucon-europe-2018-introduction-mesa.pdf
* https://wiki.st.com/stm32mpu/wiki/DRM_KMS_overview#modetest_-28DRM-2FKMS_test_tool-29\
* https://www.reddit.com/r/linux/comments/36zcsw/a_brief_introduction_to_the_linux_graphics_stack/
* https://www.youtube.com/watch?v=WaF7mVhnhtE&t=500s
* https://www.youtube.com/watch?v=wjAJmqwg47k
* https://www.youtube.com/watch?v=nau2dgdXWOk
* https://conf.linuxappsummit.org/event/1/contributions/11/attachments/6/7/LAS_2020.pdf
* https://www.youtube.com/watch?v=lEUCgGLZYo8
* https://elinux.org/images/4/42/Elce-2019-gfx-101-boris.pdf