Userspace drivers in the X server to handle communication with graphics hardware in the Xlib 2D graphics stack. Considered legacy and is mentioned for historical reasons.

DDX included drivers for graphics and HID as separate, loadable modules for the X server.