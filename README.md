# Linux Library

Repository for information of what I have learned regarding advanced information about Linux (and FOSS generally). Notes on the Linux graphics stack, how to create device drivers, etc.

This repository is designed to be navigated wit Obsidian. In the future I will make it navigable through the GitLab page. The end goal would be to turn this into a more complete version of this textbook:
https://people.freedesktop.org/~marcheu/linuxgraphicsdrivers.pdf

There are complimentary repositories for code and reference implementations for the information in this repository. Here is a borked list:

surafel911/drm-playground
surafel911/vulkan-dev
