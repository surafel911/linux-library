Each major subsystem has its own tree and designated maintainer(s). Subsystems may have multiple maintainers. You can find a list of subsystems and their maintainers in the [MAINTAINERS file](https://www.kernel.org/doc/linux/MAINTAINERS) in the kernel source.

# Mailing Lists

The development process happens almost entirely over emails. Contributors send patches through emails and the patches are discussed in emails. These emails are called _mailing lists_.

Most maintainers send an email response to contributes saying `"Applied to tree-name"` or `"Thanks Applied"` to the mailing list when they accept and commit the patches. Some maintainers have a bot patch-applied notifications with the match in them.

Almost every subsystem has a mailing list. Refer to the [Linux kernel mailing lists](http://vger.kernel.org/vger-lists.html) and the [list archives on lore.kernel.org](https://lore.kernel.org/lists.html) for a list of mailing lists and their archives. Archives are a great reference for patch discussions and history of changes.

Refer to the [kernel.org git repositories](https://git.kernel.org) for a list of kernel trees. Not all trees are on kernel.org. You can find subsystem git information and mailing lists for each of the subsystems in the MAINTAINERS file.

![[kernel-patch-flow.png]]