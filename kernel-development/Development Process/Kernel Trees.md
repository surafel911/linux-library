# Mainline Kernel Tree

The kernel tree maintained by Linus Torvalds. Where Linus releases mainline kernels and release clients

# Stable Kernel Tree

This tree is maintained by Greg Kroah-Hartman. This tree consists of stable release branches. Stable releases are based on this tree.

# Linux-Next Kernel Tree

This `linus-next` tree is the integration tree maintained by Stephen Rothwell. Code from subsystems specific kernel trees are pulled into this tree for integration testing. Here is where merge conflicts from pulling patches from multiple trees gets resolved.

Patches that you write are applied the the `linux-next` tree where (if any) merge conflicts are resolved. Give maintainers 3-7 days to find any regressions (if any) before sending a pull request to Linus.

Patches into `linux-next` are only for the next merge window, including during the merge window. Patches for the next merge window may be added after the **rc1** candidate has been released.
