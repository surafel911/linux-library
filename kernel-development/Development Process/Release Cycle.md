# Release Cycle

Linux kernel development is a continuous process - releases are time-based rather than feature-based. Releases are not held up by features and have no set date, but usually a new release comes out every 10-11 weeks

## Patches

The first step to the release cycle is creating patches. A _patch_ is a small-incremental change made to the kernel; essentially a small commit in git. A collection of patches are called a _patch set_. Patches / patch sets are basically any over commit / group of commits in any git project, they add features and remove bugs.

Contributors make patches to the kernel and submit them to [[Subsystem Maintainers|subsystem maintainer{s}]]. Once the system maintainer(s) approve of the patch set, they are applied to the respective [[Kernel Trees|kernel tree]] for that subsystem. Patches are applied to the [[Kernel Trees#Mainline Kernel Tree|mainline kernel]] during the merge window.

## Merge Windows

Linus Torvalds releases a new kernel by opening a new _merge window_. A _merge window_ is a 2-week time span where Linus pulls the code for the next release from subsystem maintainers. Subsystem maintainers send him signed pull request either during or before the merge window.  The change sets (patches) get pulled into Linus' kernel tree; at the end of the merge window he releases the first release candidate known as **rc1**. 

After **rc1** the kernel moves to bug-fix only mode, where only bug fixes and regressions are allowed. A new release client is created at the end of every week after **rc1**. Up to seven or eight release clients are created before the mainline kernel release. Once all major bug fixes and regressions are solved.

Each new cycle begins with an informal three week _quiet period_ that starts one week before the 2-week merge window. This is because subsystem maintainers are busy finalizing and sending patches to Linus. Remember that it's an informal part of the release cycle so expect slow responses from maintainers

![[kernel-release-cycle.png]]