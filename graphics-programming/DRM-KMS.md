[drm-info](https://gitlab.freedesktop.org/emersion/drm_info)
[drm-test](https://gist.github.com/uobikiemukot/c2be4d7515e977fd9e85)
[foss-north-kms](https://git.sr.ht/~emersion/foss-north-kms/tree)
[drm-setup](https://gist.github.com/techlabxe/de9184c6253f9b28cb90) 

[drm-howto](https://github.com/dvdhrm/docs/tree/master/drm-howto)
[drm-playground](https://github.com/emersion/drm-playground)
[drm-monitor](https://github.com/emersion/drm_monitor) 
[kms-quads](https://github.com/nyorain/kms-vulkan)
[drm-prime-dumb-kms](https://gist.github.com/Miouyouyou/2f227fd9d4116189625f501c0dcf0542)
[Writing DRM Engine Driver for Your GPU](https://gitlab.fmsoft.cn/VincentWei/minigui-docs/blob/master/supplementary-docs/Writing-DRM-Engine-Driver-for-Your-GPU.md)
[Writing an Open-Source GPU Driver Without the Hardware](https://www.collabora.com/news-and-blog/blog/2022/01/27/writing-an-open-source-gpu-driver-without-the-hardware/)
[kms-vulkan](https://github.com/nyorain/kms-vulkan/tree/master)
https://github.com/swaywm/wlroots/wiki/DRM-Debugging

https://medium.com/@lei.wang.sg/render-graphics-with-drm-on-linux-5ce35a932f83

https://liujunming.top/2019/10/22/libdrm-samples/

https://gist.github.com/dvdhrm/508355
https://git.ti.com/cgit/glsdk/example-applications/tree/drm-tests/drm_clone.c

https://github.com/eyelash/tutorials/blob/master/drm-gbm.c
https://gitlab.freedesktop.org/mesa/demos/-/tree/main/src/egl/opengl?ref_type=heads
https://github.com/ds-hwang/gbm_es2_demo
https://gist.github.com/Miouyouyou/89e9fe56a2c59bce7d4a18a858f389ef
