clients:
[wayland-tutorial](https://github.com/eyelash/tutorials)
[weston-examples](https://gitlab.freedesktop.org/wayland/weston/-/tree/main/clients?ref_type=heads)

compositors:
[wayland-egl](https://github.com/eyelash/tutorials/blob/master/wayland-egl.c)
[tiny wayland compositor](https://gist.github.com/danielgek/09b03838189c61575530bb64d6a53ec3)
[wayland compositor](https://github.com/eyelash/tutorials/tree/master/wayland-compositor)
[small wayland compositor](https://github.com/michaelforney/swc)
[weston compositor](https://gitlab.freedesktop.org/wayland/weston/-/tree/main/compositor?ref_type=heads)
[dwl](https://github.com/djpohly/dwl/blob/main/dwl.c)

- `wayland-info` tells you what extensions are available as a Wayland client
- `wlhax` is a client proxy to monitor requests & events from the server
- Weston has `simple-smh`, `simple-egl`, etc. sample clients on GitLab
	- `weston-debug` introspects Weston's internal details/logs

wlroots:
[tinywl](https://github.com/SimulaVR/tinywl-xwayland)
[simple-example](https://gitlab.freedesktop.org/wlroots/wlroots/-/blob/master/examples/simple.c?ref_type=heads)

vulkan:
https://github.com/umbral-software/WaylandWSIExample